const express = require('express')
const bodyParser = require('body-parser')

const app = express();
const PORT = process.env.PORT ? process.env.PORT : 56201;
const weekDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
});

app.post("/square", bodyParser.raw({type: 'text/plain'}), async (req, res) => {
    if (!req.body || req.body.length === 0)
        return res.status(400).send("Error. Empty input number")
    let number = +req.body
    if (isNaN(number)) res.sendStatus(400)
    else res.send({number: number, square: number * number})
})

app.post("/reverse", bodyParser.raw({type: 'text/plain'}), async (req, res) => {
    if (!req.body || req.body.length === 0)
        return res.status(400).send("Error. Empty input number")
    let str = req.body.toString()
    res.send(str.split("").reduce((acc, char) => char + acc, ""))
})

app.get("/date/:year/:month/:day", async (req, res) => {
    const {year, month, day} = req.params
    const currentDate = new Date()
    currentDate.setHours(0, 0, 0, 0)
    const inputDate = new Date(`${year}-${month}-${day}`)
    inputDate.setHours(0, 0, 0, 0)
    const diffInMillis = Math.abs(currentDate - inputDate)
    const diffInDays = Math.floor(diffInMillis / (1000 * 60 * 60 * 24))
    console.log(inputDate.getDay())
    res.json({
        weekDay: weekDays[inputDate.getDay()],
        isLeapYear: year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0),
        difference: diffInDays
    })
})